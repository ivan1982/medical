import React from 'react';

import API from './api';

export default class PlanList extends React.Component {
  state = {
    plans: [],
    features: [],
    error:false,
    loaded:false
  }
  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }
  componentDidCatch(error, info) {
    return { hasError: true };
  }
  componentDidMount() {
    API.get(`plans`)
      .then(res => {
        const plans = res.data;
        this.setState((state, props) => {
          return {plans: plans,loaded: true};
        });
      }).catch((err) => {
        this.setState((state, props) => {
          return {hasError: true};
        });
      })
      API.get(`features`)
      .then(res => {
        const features = res.data;
        this.setState((state, props) => {
          return {features: features,loaded: true};
        });
      }).catch((err) => {
        this.setState((state, props) => {
          return {hasError: true};
        });
      })
  }
  hasFeature(features,feature){
    let has = false;
    features.forEach(ele=>{
      if (feature.name === ele.name){
        has = true;
      }
    })
    return has;
  }
  getColumnClass(){
    return 'col col-'+Math.round(12 / this.state.plans.length); 
  }
  render() {
    if (this.state.hasError) {
      return <div className="alert alert-danger">Server Error</div>;
    }
    if (!this.state.loaded) {
      return <i className="fa fa-spinner fa-spin"></i>;
    }
    return (
        <div className="plans row">
          <div className="col col-2 text-left">
            <h4>&nbsp;</h4>
          { this.state.features.map(feature =>  
              <p key={feature.id}>{feature.name}</p>
          )}
        </div>
        <div className="col col-10">
        { this.state.plans.map(plan =>  
          <div className={this.getColumnClass()} key={plan.id}>
          <h4><span>{plan.name}</span></h4>
          { this.state.features.map(feature => {
              if(this.hasFeature(plan.features,feature)){
                return <p key={feature.id}><i className="fa fa-check"></i></p>
              }else{
                return <p key={feature.id}><i className="fa fa-times"></i></p>
              }
            })}
          <div className="price"><label><input type="radio" name="plan"/>{plan.currency} {plan.price}</label> /Monthly</div>
        </div>)}
        </div>
        </div>
    )
  }
}
