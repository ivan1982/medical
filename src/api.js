import axios from 'axios';

export default axios.create({
  baseURL: `http://47.243.158.165:1337/`,
  timeout: 5000
});