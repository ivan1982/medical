import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import PlanList from './PlanList';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';

const list =
    <div>
      <h2>Choose a plan</h2>
      <PlanList/>
    </div>
ReactDOM.render(
  list,
  document.getElementById('root')
);
